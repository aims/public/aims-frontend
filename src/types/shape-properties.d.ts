import type { Quad_Predicate } from "@rdfjs/types";
import type { InputType } from "bootstrap-vue-next";

export declare interface ShaclProperty {
  predicate: Quad_Predicate;
  labelKey: string;
  descriptionKey: string;
  localized: boolean;
  multiline?: boolean;
  type: InputType | "list" | "boolean";
  listType?: "linked" | "regular";
  searchType?: "class" | "node";
  items?: { text: string; value: string }[];
  max?: number | Quad_Predicate;
  min?: number | Quad_Predicate;
  deletable?: boolean;
}

declare interface BasicShapeProperty extends ShaclProperty {
  hidden?: boolean;
  required?: boolean;
  value: string;
}

declare interface LocaleShapeProperty extends BasicShapeProperty {
  localized: true;
  value: { [locale: string]: string };
}

declare interface NoLocaleShapeProperty extends BasicShapeProperty {
  localized: false;
}

declare interface BooleanShapeProperty extends BasicShapeProperty {
  localized: false;
  type: "boolean";
  value: boolean;
}

declare interface UrlShapeProperty extends BasicShapeProperty {
  localized: false;
  type: "url";
  deletable?: boolean;
}

declare interface ListShapeProperty extends BasicShapeProperty {
  localized: false;
  type: "list";
  listType: "regular";
  value: string[];
}

export declare type NodeShapeProperty =
  | LocaleShapeProperty
  | NoLocaleShapeProperty
  | BooleanShapeProperty
  | UrlShapeProperty
  | ListShapeProperty;
