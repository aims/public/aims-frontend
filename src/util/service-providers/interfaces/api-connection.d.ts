import type {
  ApplicationProfile,
  ApplicationProfileSearchParameters,
} from "@/types/application-profile";
import type VocabularyTerm from "@/types/vocabulary-term";
import type { Dataset } from "@rdfjs/types";
import type { PredicateObject } from "./metadata";
import type { BilingualLabel, BilingualLabels, Community } from "./labels";

declare interface ApiConnection {
  classReceiver?: (
    classUrl: string,
    pageNumber: number,
    pageSize: number,
    searchTerm?: string,
  ) => Promise<BilingualLabels | undefined>;
  getApplicationProfile(
    base_url: string,
    accessToken?: string | null,
  ): Promise<ApplicationProfile>;
  getCommunities?(): Promise<Community[]>;
  instanceReceiver?: (
    instanceUrl: string,
    classUrl?: string,
  ) => Promise<BilingualLabel | undefined>;
  searchAllApplicationProfiles(
    query: string,
    searchParameters: ApplicationProfileSearchParameters,
  ): Promise<Array<ApplicationProfile> | undefined>;
  searchApplicationProfiles(
    query: string,
    searchParameters: ApplicationProfileSearchParameters,
  ): Promise<Array<ApplicationProfile | undefined>>;
  searchClasses(query: string): Promise<Array<VocabularyTerm>>;
  searchMetadata(
    query: string,
    targetClass: string | null,
    predicateObject: Array<PredicateObject>
  ): Promise<Array<Dataset>>;
  searchVocabularyTerms(query: string): Promise<Array<VocabularyTerm>>;
  storeApplicationProfile(
    applicationProfile: ApplicationProfile,
    dataset: Dataset,
  ): Promise<ApplicationProfile | void | undefined>;
  storeMetadata(
    applicationProfile: ApplicationProfile,
    metadata: Dataset,
  ): Promise<boolean>;
}

export = ApiConnection;
