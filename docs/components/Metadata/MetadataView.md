# MetadataView

## Props

| Prop name | Description | Type    | Values | Default                                          |
| --------- | ----------- | ------- | ------ | ------------------------------------------------ |
| metadata  |             | Dataset | -      | () =&gt; factory.dataset() as unknown as Dataset |

---

```vue live
<MetadataView />
```
