# NewVersionActionModal

## Props

| Prop name | Description | Type    | Values | Default        |
| --------- | ----------- | ------- | ------ | -------------- |
| open      |             | boolean | -      | () =&gt; false |

## Events

| Event name | Properties | Description |
| ---------- | ---------- | ----------- |
| close      |            |
| save       |            |

---

```vue live
<NewVersionActionModal :open="true" />
```
