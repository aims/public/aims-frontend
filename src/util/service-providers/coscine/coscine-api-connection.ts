import type {
  ApplicationProfile,
  ApplicationProfileSearchParameters,
} from "@/types/application-profile";
import type VocabularyTerm from "@/types/vocabulary-term";
import type { Dataset } from "@rdfjs/types";
import { zazukoRequest } from "@/util/api-connections/zazuko-prefix-service";
import TerminologyService from "@/util/api-connections/terminology-service";
import ApiConnection from "@/util/service-providers/interfaces/api-connection";

import apiClient from "@coscine/api-client";
import { serializeRDFDefinitionStandard } from "@/util/serializer";
import type { PredicateObject } from "../aims/metadata";
import type { ApplicationProfileApiCreateApplicationProfileRequestRequest } from "@coscine/api-client/dist/types/Coscine.Api";
import { wrapListRequest } from "@/util/wrapListRequest";
import State from "@/types/state";
import type { BilingualLabel, BilingualLabels } from "../interfaces/labels";
import useNotificationStore from "@/store/notification";
import { AxiosError } from "axios";

class CoscineApiConnection implements ApiConnection {
  terminologyService!: TerminologyService;

  constructor(
    base_url?: string,
    terminologyServiceEndpoint = "https://service.tib.eu/ts4tib",
  ) {
    this.terminologyService = new TerminologyService(
      terminologyServiceEndpoint,
      base_url,
    );
  }

  parseName(base_url: string): string {
    let name = base_url.replace("https://purl.org/coscine/ap/", "");
    name = name[0].toUpperCase() + name.slice(1);
    if (name.endsWith("/")) {
      name = name.slice(0, Math.max(0, name.length - 1));
    }
    name = name.replace("/", " ");
    return name;
  }

  async classReceiver(
    classUrl: string,
    pageNumber: number,
    pageSize: number,
    searchTerm?: string
  ): Promise<BilingualLabels | undefined> {
    const notificationStore = useNotificationStore();
    try {
      const enInstances = await apiClient.VocabularyApi.getVocabularyInstances({
        _class: classUrl,
        language: "en",
        pageNumber,
        pageSize,
        searchTerm,
      });
      const deInstances = await apiClient.VocabularyApi.getVocabularyInstances({
        _class: classUrl,
        language: "de",
        pageNumber,
        pageSize,
        searchTerm,
      });
      return {
        dePagination: deInstances.data.pagination,
        de: deInstances.data.data?.map((instance) => {
          return {
            name: instance.displayName,
            value: instance.instanceUri,
          };
        }),
        enPagination: enInstances.data.pagination,
        en: enInstances.data.data?.map((instance) => {
          return {
            name: instance.displayName,
            value: instance.instanceUri,
          };
        }),
      };
    } catch (error) {
      // Handle other Status Codes
      notificationStore.postApiErrorNotification(error as AxiosError);
    }
  }

  async getApplicationProfile(base_url: string): Promise<ApplicationProfile> {
    const notificationStore = useNotificationStore();
    try {
      const response =
        await apiClient.ApplicationProfileApi.getApplicationProfile({
          profile: base_url,
          format: "application/ld+json",
        });
      const definition = response.data.data?.definition;
      const emptyResponse = `[{"@id":"${base_url}","@graph":[]}]`;
      if (definition === emptyResponse) {
        throw new Error("Empty Response");
      }
      return {
        name: this.parseName(base_url),
        base_url: base_url,
        definition: definition?.content,
        mimeType: definition?.type,
        state: State.PUBLISHED,
      } as ApplicationProfile;
    } catch (error) {
      // Handle other Status Codes
      notificationStore.postApiErrorNotification(error as AxiosError);
      return {
        name: this.parseName(base_url),
        base_url: base_url,
        definition: "",
        mimeType: "unknown",
        state: State.ERROR,
      } as ApplicationProfile;
    }
  }

  async instanceReceiver(
    instanceUrl: string
  ): Promise<BilingualLabel | undefined> {
    const notificationStore = useNotificationStore();
    try {
      const enInstance = await apiClient.VocabularyApi.getVocabularyInstance({
        acceptLanguage: "en",
        instance: instanceUrl,
      });
      const deInstance = await apiClient.VocabularyApi.getVocabularyInstance({
        acceptLanguage: "de",
        instance: instanceUrl,
      });
      return {
        de: {
          name: deInstance.data.data?.displayName,
          value: deInstance.data.data?.instanceUri,
        },
        en: {
          name: enInstance.data.data?.displayName,
          value: enInstance.data.data?.instanceUri,
        },
      };
    } catch (error) {
      // Handle other Status Codes
      notificationStore.postApiErrorNotification(error as AxiosError);
    }
  }

  async searchAllApplicationProfiles(
    query: string,
    searchParameters: ApplicationProfileSearchParameters
  ): Promise<Array<ApplicationProfile> | undefined> {
    const notificationStore = useNotificationStore();
    try {
      const response =
        await apiClient.ApplicationProfileApi.getApplicationProfiles({
          language: searchParameters.language,
          searchTerm: query,
          modules: true,
        });
      const applicationProfileDtos = response.data.data;

      if (applicationProfileDtos) {
        return applicationProfileDtos.map((ap) => {
          return {
            name: ap.displayName,
            base_url: ap.uri,
            definition: ap.definition?.content,
            description: ap.description,
            mimeType: ap.definition?.type,
            state: State.PUBLISHED,
          } as ApplicationProfile;
        });
      }
      return [];
    } catch (error) {
      // Handle other Status Codes
      notificationStore.postApiErrorNotification(error as AxiosError);
    }
  }

  async searchApplicationProfiles(
    query: string,
    searchParameters: ApplicationProfileSearchParameters
  ): Promise<Array<ApplicationProfile | undefined>> {
    const notificationStore = useNotificationStore();
    try {
      const applicationProfileDtos = await wrapListRequest(
        (pageNumber: number) =>
          apiClient.ApplicationProfileApi.getApplicationProfiles({
            language: searchParameters.language,
            searchTerm: query,
            modules: false,
            pageNumber,
            pageSize: 50,
          })
      );

      if (applicationProfileDtos) {
        return applicationProfileDtos.map((ap) => {
          return {
            name: ap.displayName,
            base_url: ap.uri,
            definition: ap.definition?.content,
            description: ap.description,
            mimeType: ap.definition?.type,
            state: State.PUBLISHED,
          } as ApplicationProfile;
        });
      }
    } catch (error) {
      // Handle other Status Codes
      notificationStore.postApiErrorNotification(error as AxiosError);
    }
    return [];
  }

  async searchClasses(query: string): Promise<Array<VocabularyTerm>> {
    const vocabularyTerms: VocabularyTerm[] = [];
    try {
      vocabularyTerms.push(...(await zazukoRequest(query, "class")));
    } catch (error) {
      console.error(error);
      // Catch to not have a raw dependency on Zazuko working
    }
    try {
      const response = await this.terminologyService.terminologyServiceRequest(
        query,
        "class",
      );
      vocabularyTerms.push(
        ...response.filter(
          (term) =>
            !vocabularyTerms.some(
              (vocabularyTerm) => vocabularyTerm.uri === term.uri,
            ),
        ),
      );
    } catch (error) {
      console.error(error);
      // Catch to not have a raw dependency on Terminology Service working
    }
    return vocabularyTerms;
  }

  // eslint-disable-next-line
  searchMetadata(
    query: string,
    targetClass: string | null,
    predicateObjects: Array<PredicateObject>
  ): Promise<Dataset[]> {
    const datasets: Dataset[] = [];
    return Promise.resolve(datasets);
  }

  async searchVocabularyTerms(query: string): Promise<Array<VocabularyTerm>> {
    const vocabularyTerms: VocabularyTerm[] = [];
    try {
      const response = await zazukoRequest(query, "property");
      vocabularyTerms.push(...response);
    } catch (error) {
      console.error(error);
      // Catch to not have a raw dependency on Zazuko working
    }
    try {
      const response = await this.terminologyService.terminologyServiceRequest(
        query,
        "property",
      );
      vocabularyTerms.push(
        ...response.filter(
          (term) =>
            !vocabularyTerms.some(
              (vocabularyTerm) => vocabularyTerm.uri === term.uri,
            ),
        ),
      );
    } catch (error) {
      console.error(error);
      // Catch to not have a raw dependency on Terminology Service working
    }
    return vocabularyTerms;
  }

  async storeApplicationProfile(
    applicationProfile: ApplicationProfile,
    dataset: Dataset
  ): Promise<void | undefined> {
    const mimeType = "text/turtle";
    const notificationStore = useNotificationStore();
    applicationProfile.definition = await serializeRDFDefinitionStandard(
      dataset,
      mimeType,
    );
    applicationProfile.mimeType = mimeType;
    const applicationProfileForCreationDto: ApplicationProfileApiCreateApplicationProfileRequestRequest =
      {
        applicationProfileForCreationDto: {
          uri: applicationProfile.base_url,
          definition: {
            content: applicationProfile.definition,
            type: mimeType,
          },
          name: applicationProfile.name,
        },
      };
    try {
      await apiClient.ApplicationProfileApi.createApplicationProfileRequest(
        applicationProfileForCreationDto
      );
    } catch (error) {
      // Handle other Status Codes
      notificationStore.postApiErrorNotification(error as AxiosError);
    }
  }

  // eslint-disable-next-line
  storeMetadata(applicationProfile: ApplicationProfile, metadata: Dataset): Promise<boolean> {
    throw new Error("Method not implemented.");
  }
}

export default CoscineApiConnection;
