export interface ZazukoPrefixResponse {
  iri: Graph;
  prefixed: string;
  graph: Graph;
  parts: Part[];
  prefixedSplitA: string;
  prefixedSplitB: IRISplitB;
  iriSplitA: string;
  iriSplitB: IRISplitB;
  ontologyTitle: string;
  label: IRISplitB;
  itemText: string;
}

export interface Graph {
  value: string;
}

export enum IRISplitB {
  IRISplitBTitle = "Title",
  Title = "title",
}

export interface Part {
  predicate: string;
  predicateIRI: string;
  object: ObjectClass | string;
  objectIRI?: string;
  quad: Quad;
}

export interface ObjectClass {
  value: string;
  datatype?: Graph;
  language?: Language;
}

export enum Language {
  De = "de",
  // eslint-disable-next-line unicorn/prevent-abbreviations
  El = "el",
  Empty = "",
  En = "en",
  Es = "es",
  It = "it",
  Ja = "ja",
  Nl = "nl",
}

export interface Quad {
  subject: Graph;
  predicate: Graph;
  object: ObjectClass;
  graph: Graph;
}
