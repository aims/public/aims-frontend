import zazukoPrefixes from "@zazuko/prefixes/prefixes";
import type { Prefixes } from "@zazuko/vocabularies/prefixes";

import serviceProvider from "@/util/service-providers/service-provider";

export function getPrefixes(
  base_url?: string,
): Prefixes & { [key: string]: string } {
  const customPrefixes = zazukoPrefixes;

  const basePath = serviceProvider.base_path ?? "ap/";

  if (base_url) {
    customPrefixes["vocabterms"] = base_url + "terms/";
    customPrefixes["aps"] = base_url + basePath;
  }

  return customPrefixes;
}
