export default {
  nav: {
    brand: "Metadata Profile Service",
    extrasHelp: "Help",
    extrasDisclaimer: "Disclaimer",
    extrasImprint: "Imprint",

    url: {
      extrasHelp: "https://docs.coscine.de/en/metadata/generator/about/",
      extrasDisclaimer:
        "https://git.rwth-aachen.de/coscine/docs/public/terms/-/blob/master/PrivacyPolicy.md",
      extrasImprint: "https://www.coscine.de/en/imprint/",
    },

    theme: {
      toggle: "Toggle theme",

      light: "Light",
      dark: "Dark (Beta)",
    },
  },

  ApplicationProfile: "Application Profile",

  Action: "Action",
  Create: "Create",
  Import: "Import",
  New: "New",
  Edit: "Edit",
  Copy: "Copy",
  Extend: "Extend",
  NewVersion: "New Version",

  mustNotBeClosed: "Current @.lower:{'ApplicationProfile'} must not be closed",

  EnterToken: "Enter Token",
  Search: "Search",
  Editor: "Editor",
  Graph: "Graph",
  MetadataForm: "Metadata Form",
  Preview: "Preview",
  RDF: "Code View",
  Download: "Download Code",
  DownloadComplete: "Download Code With Imports",
  Save: "Save",

  Lang: "Language",
  DE: "German",
  EN: "English",

  Cancel: "Cancel",
  Continue: "Continue",
  Submit: "Submit",
  SendToReview: "Submit for Review",

  true: "True",
  false: "False",

  CurrentlyDemo:
    "This website is currently under construction and shall only be used for testing!",

  CurrentlyMockup:
    "You are currently on the mockup page of the AIMS frontend. No changes are persistent!",

  availableapplicationprofiles: "Available @:{'ApplicationProfile'}s",
  vocabulary: "Vocabulary Terms",
  detailview: "Detail View (Fields)",
  properties: "Field Properties",

  searchAP: "Search @:{'ApplicationProfile'}s",
  searchVoc: "Search Vocabulary Terms",

  created: "Creation Date",
  createdDescription:
    "When the @.lower:{'ApplicationProfile'} has been created (dcterms:created)",
  creator: "Creator",
  creatorDescription:
    "The person who created this @.lower:{'ApplicationProfile'} (dcterms:creator)",
  creatorFilterDescription: "Filter with the creator.",
  dateFilters: "Date Filters",
  dateFiltersDescription: "Enter a Minimum and Maximum Date.",
  description: "Description",
  descriptionDescription:
    "Description of the @.lower:{'ApplicationProfile'} (dcterms:description)",
  discipline: "Discipline",
  label: "Title",
  labelDescription:
    "Name of the @.lower:{'ApplicationProfile'} (dcterms:title)",
  node: "Inherits from",
  nodeDescription:
    "With the \"Expand\" option, the underlying @.lower:{'ApplicationProfile'} is displayed here (sh:node)",
  license: "License",
  licenseDescription: "License definition (dcterms:license)",
  subject: "Subject",
  subjectDescription:
    "Area in which the @.lower:{'ApplicationProfile'} fits in (dcterms:subject)",
  subjectFilterDescription: "Filter with the state.",
  targetClass: "Target Class",
  targetClassDescription:
    "Copy the URL of the @.lower:{'ApplicationProfile'} (see above) or specify class of an ontology (sh:targetClass)",
  closed: "Closed",
  closedDescription:
    "Open @.lower:{'ApplicationProfile'}s may be extended, closed @.lower:{'ApplicationProfile'}s cannot. Extending an @.lower:{'ApplicationProfile'} creates a new @.lower:{'ApplicationProfile'} that inherits the existing one. (sh:closed)",
  url: "@:{'ApplicationProfile'} URL",
  urlDescription:
    "Editable, a unique name should be specified for the profile.",
  state: "State of the @:{'ApplicationProfile'}",
  stateDescription:
    "Determine if the @.lower:{'ApplicationProfile'} should be in a draft state or published (not changeable).",
  stateFilterDescription: "Filter with the status.",
  draft: "Draft",
  publish: "Published",
  necessaryBaseUrl: "The Base URL '{base_url}' is necessary.",

  metadataForm: "Metadata Form of {ap}",
  metadataView: "Metadata View",

  storeModalTitle: "Save New @:{'ApplicationProfile'}: {apName}",
  editModalTitle: "Edit @:{'ApplicationProfile'}: {apName}",

  defaultStoreToastDescription:
    "Your @.lower:{'ApplicationProfile'} has been saved!",
  defaultStoreToastTitle: "Saved @:{'ApplicationProfile'}",

  defaultStoreFailedToastDescription:
    "Storing of your @.lower:{'ApplicationProfile'} failed!",
  defaultStoreFailedToastTitle: "Storing failed",

  defaultStoringToastDescription: "Storing ...",
  defaultStoringToastTitle: "Storing @:{'ApplicationProfile'}",

  apiError: {
    unauthorized: {
      title: "An error occurred",
      body: "Authorization failed: Your token in the Authorization Header is invalid, expired, or missing. Please provide a valid Bearer Token.",
    },
    general: {
      title: "An error occurred",
      body: "An error occurred. Please try again. If the error persists, please contact your organization.",
    },
    specific: {
      title: "An error occurred",
      body: "Something went wrong {error}. Please try again. If the issue persists, contact your organization and provide the following Trace ID: {traceId}.",
    },
  },

  defaultStoreMetadataToastDescription: "Your metadata has been saved!",
  defaultStoreMetadataToastTitle: "Saved Metadata",

  defaultStoreMetadataFailedToastDescription:
    "Storing of your metadata failed!",
  defaultStoreMetadataFailedToastTitle: "Storing failed",

  defaultStoringMetadataToastDescription: "Storing ...",
  defaultStoringMetadataToastTitle: "Storing Metadata",

  emptyApplicationProfileList:
    "No fitting @.lower:{'ApplicationProfile'} has been found for the given input.",

  emptyVocabList:
    "No fitting vocabulary term has been found for the given input.",
  addCustomTerm: "Add a custom term",

  currentAP: "Current @:{'ApplicationProfile'}",
  inheritedAPs: "Inherited @:{'ApplicationProfile'}s",
  noNameSet: "(No Name Set)",
  emptyPathList:
    "No vocabulary term has been added yet to this @.lower:{'ApplicationProfile'}. You can add a vocabulary term by dragging it from the \"@:{'vocabulary'}\" column into this @.lower:{'ApplicationProfile'}.",

  pleaseSelectOption: "-- Please select an option --",

  searchSelectNoOptions:
    "There are no options to select. You can either use a more concrete query or enter your own value and add it by pressing the 'Enter' key.",

  propertyTypesLabel: "Property Type",
  propertyTypesDescription: 'Type of property - "Data type" by default',

  addEntry: "Add Entry",

  actions: {
    copyModalTitle: "Copy @:{'ApplicationProfile'}",
    copyModalDescription:
      "This action will create a new @.lower:{'ApplicationProfile'} which acts as a copy of the previously selected @.lower:{'ApplicationProfile'}. If you were editing an @.lower:{'ApplicationProfile'}, continuing will keep your progress in a separate draft.",
    createModalTitle: "Create New @:{'ApplicationProfile'}",
    createModalDescription:
      "This action will create a new empty @.lower:{'ApplicationProfile'}. If you were editing an @.lower:{'ApplicationProfile'}, continuing will keep your progress in a separate draft.",
    extendModalTitle: "Extend @:{'ApplicationProfile'}",
    extendModalDescription:
      "This action will create a new @.lower:{'ApplicationProfile'} which extends the previously selected @.lower:{'ApplicationProfile'}. If you were editing an @.lower:{'ApplicationProfile'}, continuing will keep your progress in a separate draft.",
    importModalTitle: "Import @:{'ApplicationProfile'}",
    importModalDescription:
      "This action will import an @.lower:{'ApplicationProfile'}. Please click on the button below to select your file. If you were editing an @.lower:{'ApplicationProfile'}, continuing will keep your progress in a separate draft.",
    importModalDropPlaceholder: "Drop @.lower:{'ApplicationProfile'} here...",
    importModalFilePlaceholder:
      "Choose an @.lower:{'ApplicationProfile'} or drop it here...",
    newVersionModalTitle: "New Version of @:{'ApplicationProfile'}",
    newVersionModalDescription:
      "This action will create a new version of the previously selected @.lower:{'ApplicationProfile'}. If you were editing an @.lower:{'ApplicationProfile'}, continuing will keep your progress in a separate draft.",
    removeDraftModalTitle: "Delete Draft",
    removeDraftModalDescription: 'This action will delete the draft "{name}".',
  },

  administrativeVisible: "Show Administrative Properties",
  propertyPairVisible: "Show Properties For Property Pairs",
  qualifiedVisible: "Show Properties For Qualified Shapes",
  stringVisible: "Show String-based Properties",
  valueVisible: "Show Value-based Properties",

  inheritanceVisible: "Show Relationships",

  shacl: {
    class: {
      label: "Class",
      description:
        "Supplied value has to be an instance of the class, e.g. http://purl.org/dc/dcmitype/ (sh:class)",
    },
    datatype: {
      label: "Datatype",
      description:
        "Data type/format of the expected input values (default: string) based on the XML Schema datatypes (sh:datatype)",
    },
    defaultValue: {
      label: "Default Value",
      description: "Default value in the field (sh:defaultValue)",
    },
    description: {
      label: "Description",
      description: "Description of the property (sh:description)",
    },
    disjoint: {
      label: "Disjoint To",
      description:
        "The property the provided value should be disjoint to (sh:disjoint)",
    },
    equals: {
      label: "Equal To",
      description:
        "The property the provided value should be equal to (sh:equals)",
    },
    flags: {
      label: "Regex Flags",
      description: "Regex flags (sh:flags)",
    },
    hasValue: {
      label: "Value",
      description: "Explicit expected value (sh:hasValue)",
    },
    in: {
      label: "List",
      description: "List of string values, e.g. Apple, Banana, Lemon (sh:in)",
    },
    lessThan: {
      label: "Less Than",
      description:
        "The property the provided value should be less than (sh:lessThan)",
    },
    lessThanOrEquals: {
      label: "Less Than Or Equals To",
      description:
        "The property the provided value should be less than or equals to (sh:lessThanOrEquals)",
    },
    maxCount: {
      label: "Maximum Possible Entries",
      description:
        "Maximum number of values that can be specified by the user (sh:maxCount)",
    },
    maxExclusive: {
      label: "Maximum Exklusive",
      description:
        "Set the range of values for a term, Maximum Exklusive (sh:maxExclusive)",
    },
    maxInclusive: {
      label: "Maximum Inklusive",
      description:
        "Set the range of values for a term, Maximum Inklusive (sh:maxInclusive)",
    },
    maxLength: {
      label: "Maximum Length",
      description: "Maximum occurrence of the value (sh:maxLength)",
    },
    message: {
      label: "Error Message",
      description: "Error message, displayed upon a general error (sh:message)",
    },
    minCount: {
      label: "Minimum Required Entries",
      description:
        "Minimum number of values that have to be specified by the user (sh:minCount)",
    },
    minExclusive: {
      label: "Minimum Exclusive",
      description:
        'Set the range of values for a term, Minimum Exclusive (Example: if Min. Exclusive = "1"  then value "1" is disallowed) (sh:minExclusive)',
    },
    minInclusive: {
      label: "Minimum Inclusive",
      description:
        "Set the range of values for a term, Minimum Inclusive (sh:minInclusive)",
    },
    minLength: {
      label: "Minimum Length",
      description: "Minimum occurrence of the value (sh:minLength)",
    },
    name: {
      label: "Field Name",
      description: "Display name of the field (sh:name)",
    },
    node: {
      label: "Satisfies Profile (always) (node)",
      description:
        "Points to another validating @.lower:{'ApplicationProfile'}, e.g. https://purl.org/coscine/ap/engmeta (sh:node)",
    },
    nodeKind: {
      label: "Node Kind",
      description: "Currently not implemented (sh:nodeKind)",
    },
    order: {
      label: "Position On Metadata Form",
      description:
        "Display order of the property among the other properties (sh:order)",
    },
    path: {
      label: "Term IRI",
      description: "Path/URL to the definition of the term (sh:path)",
    },
    pattern: {
      label: "Regex Pattern",
      description: "Regex Pattern for the validation of the value (sh:pattern)",
    },
    qualifiedMaxCount: {
      label: "Max Count of Qualified @:{'ApplicationProfile'}",
      description:
        "Set the maximum amount of the @.lower:{'ApplicationProfile'} occurring (sh:qualifiedMaxCount)",
    },
    qualifiedMinCount: {
      label: "Min Count of Qualified @:{'ApplicationProfile'}",
      description:
        "Set the minimum amount of the @.lower:{'ApplicationProfile'} occurring (sh:qualifiedMinCount)",
    },
    qualifiedValueShape: {
      label: "Satisfies Profile (m-n times) (qualified)",
      description:
        "URL/Definition of a conforming qualifying @.lower:{'ApplicationProfile'} (sh:qualifiedValueShape)",
    },
    qualifiedValueShapesDisjoint: {
      label: "Qualified Value Shapes Disjoint",
      description:
        "Turn on/off whether qualified value shapes are allowed to be disjoint (sh:qualifiedValueShapesDisjoint)",
    },
    severity: {
      label: "Severity",
      description: "Severity of the error (sh:severity)",
    },
    singleLine: {
      label: "Single Line",
      description:
        "If the metadata field should be in a single line (dash:singleLine)",
    },
  },

  communitiesLabel: "Communities",
  communitiesDescription: "Please select the community to send for review",
  communitiesFilterDescription:
    "Select a community that the @:{'ApplicationProfile'} should belong to.",

  aims: {
    afterStoreModalTitle: "Your Access Token",
    afterStoreModalDescription:
      "If you want to edit or publish your @.lower:{'ApplicationProfile'}, you need an access token. Because of this, it is important to note down the following token:",

    fundingStatement: "NFDI4Ing is supported by DFG under project number",

    reviewModalTitle: "Submit @:{'ApplicationProfile'} for review",
    reviewModalDescription:
      "Please select the community to submit the @.lower:{'ApplicationProfile'} to:",
    reviewedModalDescription:
      "The request was sent for review. You can track the progress with the following merge request:",
  },

  coscine: {
    authorizationModalTitle: "Enter your Bearer token",
    storeDescription:
      "You are going to request for submission of your customized @.lower:{'ApplicationProfile'}. " +
      "You will receive an email including a link to your GitLab merge request to help you follow up on the status of your application.",
    storeToastDescription:
      "An email has been sent to you. You will be contacted about the status of your request.",
    storeToastTitle: "@:{'ApplicationProfile'} Submitted",
    tokenLabel: "Bearer Token",
    tokenHere: "here",
    tokenNotice: "You can generate a token {0}.",
    tokenUrl: "https://coscine.rwth-aachen.de/user/",
  },
};
