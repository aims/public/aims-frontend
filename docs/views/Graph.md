# GraphView

## Props

| Prop name                        | Description | Type                                                                                                                                                                           | Values | Default |
| -------------------------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------ | ------- |
| applicationProfile               |             | ApplicationProfile                                                                                                                                                             | -      |         |
| completeApplicationProfile       |             | {<br/> definition: Dataset;<br/> inheritedParts: Record&lt;<br/> string,<br/> {<br/> applicationProfile: ApplicationProfile;<br/> definition: Dataset;<br/> }<br/> &gt;;<br/>} | -      |         |
| sessionStoredApplicationProfiles |             | number                                                                                                                                                                         | -      | 0       |
| token                            |             | string                                                                                                                                                                         | -      | ""      |

## Events

| Event name                        | Properties | Description |
| --------------------------------- | ---------- | ----------- |
| emptyApplicationProfile           |            |
| selectApplicationProfile          |            |
| switchDisplayedApplicationProfile |            |

---

```vue live
<GraphView
  :applicationProfile="Default Example Usage"
  :completeApplicationProfile="Default Example Usage"
/>
```
