# AimsReviewModal

## Props

| Prop name          | Description | Type               | Values | Default        |
| ------------------ | ----------- | ------------------ | ------ | -------------- |
| applicationProfile |             | ApplicationProfile | -      |                |
| open               |             | boolean            | -      | () =&gt; false |

## Events

| Event name | Properties | Description |
| ---------- | ---------- | ----------- |
| close      |            |
| save       |            |

---

```vue live
<AimsReviewModal :applicationProfile="Default Example Usage" :open="true" />
```
