import ServiceProvider from "@/util/service-providers/interfaces/service-provider";
import CoscineApiConnection from "@/util/service-providers/coscine/coscine-api-connection";
import coscineLogo from "@/assets/rwth_coscine_weiss_rgb.svg";

import CoscineAuthorizationModal from "@/components/ServiceProviders/Coscine/CoscineAuthorizationModal.vue";
import CoscineStoreDescription from "@/components/ServiceProviders/Coscine/CoscineStoreDescription.vue";
import CoscineStoreToast from "@/components/ServiceProviders/Coscine/CoscineStoreToast.vue";

const base_url = "https://purl.org/coscine/";

class CoscineServiceProvider implements ServiceProvider {
  apiConnection = new CoscineApiConnection(base_url);
  base_url = base_url;
  base_path = "ap/";
  can_store_metadata = false;
  force_base_url = true;
  logo = {
    class: "coscineLogo",
    location: coscineLogo,
  };
  name = "Coscine";
  uiElements = {
    AuthorizationModal: CoscineAuthorizationModal,
    StoreDescription: CoscineStoreDescription,
    StoreToast: CoscineStoreToast,
  };

  applicable(): boolean {
    return (
      window.location.hostname.includes("devlef.campus.rwth-aachen.de") ||
      window.location.hostname.includes("coscine.rwth-aachen") ||
      window.location.hostname.includes("coscine.de") ||
      window.location.hostname.includes("coscine.dev")
    );
  }
  authorized(): boolean {
    return window.coscine.authorization.bearer !== "";
  }
  getToken(): string {
    const urlParameters = new URLSearchParams(window.location.search);
    const token = urlParameters.get("token");
    if (token === null) {
      const localStorageToken = localStorage.getItem(
        "coscine.authentification.bearer",
      );
      if (localStorageToken !== null) {
        window.coscine.authorization.bearer = localStorageToken;
      }
    } else {
      window.coscine.authorization.bearer = token;
    }
    return window.coscine.authorization.bearer;
  }
}

export default CoscineServiceProvider;
