# Contributing to the AIMS Frontend

Thank you for your interest in contributing to the AIMS Frontend! This guide will walk you through setting up the project, understanding its structure, and running different service providers.

## Install

You can set up the AIMS Frontend either locally or within a development container.

### Local Setup

1. **Clone the repository**: Start by cloning the repository to your local machine.

    ```bash
    git clone https://git.rwth-aachen.de/coscine/frontend/apps/aimsfrontend.git
    ```

2. **Navigate to the project directory**:

    ```bash
    cd aimsfrontend
    ```

3. **Install dependencies** using Yarn:

    ```bash
    yarn install
    ```

### Development Container Setup

If you prefer using a development container (e.g., using Docker or VS Code's Remote Containers extension), ensure you have the necessary configurations in place (`Dockerfile` or `.devcontainer/devcontainer.json`).

1. **Build the container**:

    ```bash
    docker build -t aimsfrontend .
    ```

2. **Run the container**:

    ```bash
    docker run -it -p 8080:8080 aimsfrontend
    ```

Or, if using VS Code's Remote Containers, simply open the project folder in VS Code and click on `Reopen in Container` when prompted.

## Running the application

Use the following command to run the application on port `9743` in mockup mode:

```
yarn dev
```

## Run Different Service Providers

The AIMS Frontend can interact with various service providers. These service providers are implemented in the `src\util\service-providers` directory and each follow the [service provider interface](./src/util/service-providers/interfaces/service-provider.d.ts).

The service providers all implement the following method to declare for which specific URL space they are responsible for:

```typescript
  applicable(): boolean {
    return (
      window.location.hostname.includes("myAmazingUrl.com")
    );
  }
```

You can manually select the loaded service-provider by changing the return value in [src\util\service-providers\service-provider.ts](./src/util/service-providers/service-provider.ts).

```typescript
// Example

// Replace:
const returnServiceProvider: ServiceProvider = serviceProvider;

// With (Change AIMSServiceProvider to your service provider):
const returnServiceProvider: ServiceProvider = new AIMSServiceProvider();
```

## Code Structure

The AIMS Frontend project follows a modular structure to keep the code organized and maintainable. Here's an overview:

* **src/**: The source directory contains all the Vue components, pages, and utilities.
* **src/assets/**: Assets like images used in the project.
* **src/classes/**: Used classes in the project.
* **src/components/**: Reusable Vue components, grouped by domain.
* **src/data/**: Data for the mockup mode.
* **src/locale/**: English and German texts.
* **src/plugins/**: Used plugins.
* **src/router/**: Vue Router configurations.
* **src/store/**: Pinia store, including local storage data.
* **src/types/**: TypeScript type definitions and interfaces.
* **src/util/**: Service Providers and utility functions.
* **src/views/**: Vue components that represent different pages.

## Contributing Changes

When contributing changes, ensure you follow the project's coding conventions and include necessary tests. Use Yarn scripts to lint and format your code:

```
yarn lint
yarn lint:fix
```

Use Yarn scripts to test your code:

```
yarn test
```

## Submitting a Pull Request
After making your changes, push your branch to GitHub and open a pull request against the main branch. Include a clear description of the changes and any relevant issue numbers.

Thank you for contributing to the AIMS Frontend!
