# SubmissionActions

## Props

| Prop name                  | Description | Type                                                                                                                                                                           | Values | Default |
| -------------------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------ | ------- |
| applicationProfile         |             | ApplicationProfile                                                                                                                                                             | -      |         |
| completeApplicationProfile |             | {<br/> definition: Dataset;<br/> inheritedParts: Record&lt;<br/> string,<br/> {<br/> applicationProfile: ApplicationProfile;<br/> definition: Dataset;<br/> }<br/> &gt;;<br/>} | -      |         |
| currentDefinition          |             | Dataset                                                                                                                                                                        | -      |         |
| definition                 |             | Dataset                                                                                                                                                                        | -      |         |
| readonly                   |             | boolean                                                                                                                                                                        | -      | false   |

## Events

| Event name                | Properties | Description |
| ------------------------- | ---------- | ----------- |
| updateDefinition          |            |
| storedApplicationProfiles |            |
| stateChange               |            |

---

```vue live
<SubmissionActions
  :applicationProfile="Default Example Usage"
  :completeApplicationProfile="Default Example Usage"
  :currentDefinition="Default Example Usage"
  :definition="Default Example Usage"
/>
```
