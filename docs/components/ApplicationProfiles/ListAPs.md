# ListAPs

## Props

| Prop name                  | Description | Type                            | Values | Default   |
| -------------------------- | ----------- | ------------------------------- | ------ | --------- |
| applicationProfiles        |             | ApplicationProfile[]            | -      |           |
| searchRunning              |             | boolean                         | -      | false     |
| selectedApplicationProfile |             | ApplicationProfile \| undefined | -      | undefined |

## Events

| Event name               | Properties | Description |
| ------------------------ | ---------- | ----------- |
| emptyApplicationProfile  |            |
| selectApplicationProfile |            |

---

```vue live
<ListAPs :applicationProfiles="Default Example Usage" />
```
