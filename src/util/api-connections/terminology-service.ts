import { TerminologyServiceResponse } from "@/types/terminology-service-response";
import VocabularyTerm from "@/types/vocabulary-term";
import axios from "axios";
import { getPrefixes } from "@/util/prefixes";
import { provideAdditionalInformation } from "../additionalInformationProvider";

class TerminologyService {
  endpoint: string;
  base_url?: string;

  constructor(endpoint: string, base_url?: string) {
    this.endpoint = endpoint;
    this.base_url = base_url;
  }

  public async terminologyServiceRequest(
    query: string,
    type: "class" | "property",
  ): Promise<Array<VocabularyTerm>> {
    const response = await axios.get(
      `${this.endpoint}/api/search?q=${encodeURIComponent(
        query,
      )}&groupField=iri&start=0&type=${type}`,
    );
    const terminologyServiceResponse =
      response.data as TerminologyServiceResponse;

    const prefixes = getPrefixes(this.base_url);
    const usedPrefixes: string[] = [];
    for (const document_ of terminologyServiceResponse.response.docs) {
      for (const prefixKey of Object.keys(prefixes)) {
        if (document_.iri.includes(prefixes[prefixKey])) {
          usedPrefixes.push(prefixKey);
          break;
        }
      }
    }

    return await Promise.all(
      terminologyServiceResponse.response.docs.map(async (document_) => {
        let prefix;

        for (const prefixKey of Object.keys(prefixes)) {
          if (document_.iri.includes(prefixes[prefixKey])) {
            prefix = prefixKey;
            break;
          }
        }

        const additionalInformation = await provideAdditionalInformation(
          prefixes,
          document_.iri,
          prefix,
          usedPrefixes,
        );

        return {
          label: document_.label,
          prefix: prefix,
          short_form: document_.short_form,
          uri: document_.iri,
          description: document_.description
            ? document_.description.length > 1
              ? { en: document_.description[1], de: document_.description[0] }
              : { en: document_.description[0], de: document_.description[0] }
            : additionalInformation.description,
          ranges: additionalInformation.ranges,
        };
      }),
    );
  }
}

export default TerminologyService;
