enum State {
  DRAFT,
  PUBLISHED,
  ERROR,
}

export default State;
