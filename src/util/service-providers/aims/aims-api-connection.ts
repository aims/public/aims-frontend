import type {
  ApplicationProfile,
  ApplicationProfileSearchParameters,
} from "@/types/application-profile";
import type VocabularyTerm from "@/types/vocabulary-term";
import type { Dataset } from "@rdfjs/types";

import TerminologyService from "@/util/api-connections/terminology-service";
import { zazukoRequest } from "@/util/api-connections/zazuko-prefix-service";
import ApiConnection from "@/util/service-providers/interfaces/api-connection";

import axios from "axios";
import {
  Configuration,
  MetadataApiFactory,
  type PredicateObject,
} from "./metadata";
import {
  parseRDFDefinition,
  serializeRDFDefinition,
} from "@/util/basicLinkedData";
import type { Community } from "../interfaces/labels";

class AIMSApiConnection implements ApiConnection {
  endpoint: string;
  metadataApi: ReturnType<typeof MetadataApiFactory>;
  terminologyService: TerminologyService;

  constructor(
    base_url?: string,
    endpoint = "https://pg4aims.ulb.tu-darmstadt.de/AIMS",
    metadataEndpoint = "https://api.aims.otc.coscine.dev",
    terminologyServiceEndpoint = "https://service.tib.eu/ts4tib",
  ) {
    this.endpoint = endpoint;
    this.metadataApi = MetadataApiFactory(
      new Configuration(),
      metadataEndpoint,
      axios,
    );
    this.terminologyService = new TerminologyService(
      terminologyServiceEndpoint,
      base_url,
    );
  }

  async getApplicationProfile(
    base_url: string,
    accessToken?: string | null,
  ): Promise<ApplicationProfile> {
    const response = await axios.get(
      `${this.endpoint}/application-profiles/${encodeURIComponent(
        base_url,
      )}?includeDefinition=true` +
        (accessToken ? `&accessToken=${accessToken}` : ""),
    );
    const applicationProfile: ApplicationProfile = response.data;
    // Deal with incorrectly labeled property from the AIMS backend
    if ("children" in applicationProfile) {
      applicationProfile.inheritedFrom =
        applicationProfile.children as string[];
    }
    return applicationProfile;
  }

  async searchAllApplicationProfiles(
    query: string,
    searchParameters: ApplicationProfileSearchParameters,
  ): Promise<Array<ApplicationProfile>> {
    return await this.searchApplicationProfiles(query, searchParameters);
  }

  async searchApplicationProfiles(
    query: string,
    searchParameters: ApplicationProfileSearchParameters,
  ): Promise<Array<ApplicationProfile>> {
    let requestUrl = `${
      this.endpoint
    }/application-profiles/?query=${encodeURIComponent(
      query,
    )}&language=${searchParameters.language.toUpperCase()}&includeDefinition=true`;
    if (searchParameters.community) {
      requestUrl += "&community=" + searchParameters.community;
    }
    if (searchParameters.creator) {
      requestUrl += "&creator=" + searchParameters.creator;
    }
    if (searchParameters.from) {
      requestUrl += "&from=" + searchParameters.from;
    }
    if (searchParameters.state) {
      requestUrl += "&state=" + searchParameters.state;
    }
    if (searchParameters.subject) {
      requestUrl += "&subject=" + searchParameters.subject;
    }
    if (searchParameters.to) {
      requestUrl += "&to=" + searchParameters.to;
    }
    const response = await axios.get(requestUrl);
    const applicationProfiles: Array<ApplicationProfile> = response.data;
    for (const applicationProfile of applicationProfiles) {
      // Deal with incorrectly labeled property from the AIMS backend
      if ("children" in applicationProfile) {
        applicationProfile.inheritedFrom =
          applicationProfile.children as string[];
      }
    }
    return applicationProfiles;
  }

  async searchClasses(query: string): Promise<Array<VocabularyTerm>> {
    const vocabularyTerms: VocabularyTerm[] = [];
    try {
      vocabularyTerms.push(...(await zazukoRequest(query, "class")));
    } catch (error) {
      console.error(error);
      // Catch to not have a raw dependency on Zazuko working
    }
    try {
      const response = await this.terminologyService.terminologyServiceRequest(
        query,
        "class",
      );
      vocabularyTerms.push(
        ...response.filter(
          (term) =>
            !vocabularyTerms.some(
              (vocabularyTerm) => vocabularyTerm.uri === term.uri,
            ),
        ),
      );
    } catch (error) {
      console.error(error);
      // Catch to not have a raw dependency on Terminology Service working
    }
    return vocabularyTerms;
  }

  async searchMetadata(
    query: string,
    targetClass: string | null,
    predicateObjects: Array<PredicateObject>,
  ): Promise<Dataset[]> {
    const response = await this.metadataApi.metadataPostAll(
      predicateObjects,
      undefined,
      undefined,
      query,
      targetClass ?? undefined,
    );
    const metadataList = response.data;
    const datasets: Dataset[] = [];
    for (const metadata of metadataList) {
      datasets.push(await parseRDFDefinition(metadata, "text/turtle"));
    }
    return datasets;
  }

  async searchVocabularyTerms(query: string): Promise<Array<VocabularyTerm>> {
    const vocabularyTerms: VocabularyTerm[] = [];
    try {
      const response = await zazukoRequest(query, "property");
      vocabularyTerms.push(...response);
    } catch (error) {
      console.error(error);
      // Catch to not have a raw dependency on Zazuko working
    }
    try {
      const response = await this.terminologyService.terminologyServiceRequest(
        query,
        "property",
      );
      vocabularyTerms.push(
        ...response.filter(
          (term) =>
            !vocabularyTerms.some(
              (vocabularyTerm) => vocabularyTerm.uri === term.uri,
            ),
        ),
      );
    } catch (error) {
      console.error(error);
      // Catch to not have a raw dependency on Terminology Service working
    }
    return vocabularyTerms;
  }

  async storeApplicationProfile(
    applicationProfile: ApplicationProfile,
    dataset: Dataset,
  ): Promise<ApplicationProfile | void> {
    const mimeType = "text/turtle";
    applicationProfile.definition = await serializeRDFDefinition(
      dataset,
      mimeType,
    );
    applicationProfile.mimeType = mimeType;
    const response = await axios.put(
      `${this.endpoint}/application-profiles`,
      applicationProfile,
    );
    return response.data;
  }

  async storeMetadata(
    applicationProfile: ApplicationProfile,
    metadata: Dataset,
  ): Promise<boolean> {
    await this.metadataApi.metadataPost(
      metadata.toCanonical(),
      applicationProfile.base_url,
    );
    return true;
  }

  async getCommunities(): Promise<Community[]> {
    const response = await axios.get(`${this.endpoint}/Communities`);
    return response.data;
  }

  async sendApplicationProfileForReview(
    applicationProfile: ApplicationProfile,
    community: string,
  ): Promise<string> {
    const response = await axios.get(
      `${this.endpoint}/application-profiles/SubmitToCommunity?uri=${applicationProfile.base_url}&community=${community}`,
    );
    return response.data;
  }
}

export default AIMSApiConnection;
