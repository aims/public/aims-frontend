# HomeView

## Props

| Prop name                        | Description | Type               | Values | Default |
| -------------------------------- | ----------- | ------------------ | ------ | ------- |
| applicationProfile               |             | ApplicationProfile | -      |         |
| definition                       |             | Dataset            | -      |         |
| interactive                      |             | boolean            | -      | true    |
| queryingApplicationProfile       |             | boolean            | -      | false   |
| readonly                         |             | boolean            | -      | false   |
| sessionStoredApplicationProfiles |             | number             | -      | 0       |
| token                            |             | string             | -      | ""      |

## Events

| Event name               | Properties | Description |
| ------------------------ | ---------- | ----------- |
| emptyApplicationProfile  |            |
| selectApplicationProfile |            |

---

```vue live
<HomeView
  :applicationProfile="Default Example Usage"
  :definition="Default Example Usage"
/>
```
