import { useLocalStorage } from "@vueuse/core";
import { computed } from "vue";

export const initCalculateColumnSizes = (
  defaultColumnSizes: number[],
  growOrder: number[],
) => {
  return (collapsed: number[]) => {
    const columnSizes = [...defaultColumnSizes];
    let toReadd = 0;
    for (const entry of collapsed) {
      toReadd += columnSizes[entry] - 1;
      columnSizes[entry] = 1;
    }

    if (columnSizes.some((size) => size > 1)) {
      let currentGrowIndex = 0;
      while (toReadd !== 0) {
        const currentGrowNumber =
          growOrder[currentGrowIndex % growOrder.length];
        if (columnSizes[currentGrowNumber] !== 1) {
          columnSizes[currentGrowNumber]++;
          toReadd--;
        }
        currentGrowIndex++;
      }
    }

    return columnSizes;
  };
};

export const useColumns = (
  defaultColumnSizes: number[],
  growOrder: number[],
  label: string,
) => {
  const calculateColumnSizes = initCalculateColumnSizes(
    defaultColumnSizes,
    growOrder,
  );
  const collapsedList = useLocalStorage<number[]>(
    `aims.views.${label}Collapse`,
    [],
  );

  const handleCollapse = (collapsedRow: number) => {
    if (collapsedList.value.includes(collapsedRow)) {
      collapsedList.value.splice(collapsedList.value.indexOf(collapsedRow), 1);
    } else {
      collapsedList.value.push(collapsedRow);
    }
  };

  const columnSizes = computed(() => {
    return calculateColumnSizes(collapsedList.value);
  });

  return { collapsedList, columnSizes, handleCollapse };
};
