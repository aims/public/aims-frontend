import type { ShaclProperty } from "@/types/shape-properties";
import factory from "rdf-ext";
import type { Quad_Predicate } from "@rdfjs/types";
import { prefixes } from "./linkedData";
import { dataTypes, nodeKinds, severities } from "@/util/shaclTypes";

export const shaclDatatypeProperty: ShaclProperty = {
  predicate: factory.namedNode(prefixes.sh + "datatype") as Quad_Predicate,
  labelKey: "shacl.datatype.label",
  descriptionKey: "shacl.datatype.description",
  localized: false,
  items: dataTypes,
  type: "url",
};

export const defaultShaclProperties: ShaclProperty[] = [
  {
    predicate: factory.namedNode(prefixes.sh + "name") as Quad_Predicate,
    labelKey: "shacl.name.label",
    descriptionKey: "shacl.name.description",
    localized: true,
    type: "text",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "minCount") as Quad_Predicate,
    labelKey: "shacl.minCount.label",
    descriptionKey: "shacl.minCount.description",
    localized: false,
    max: factory.namedNode(prefixes.sh + "maxCount"),
    min: 0,
    type: "number",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "maxCount") as Quad_Predicate,
    labelKey: "shacl.maxCount.label",
    descriptionKey: "shacl.maxCount.description",
    localized: false,
    min: factory.namedNode(prefixes.sh + "minCount"),
    type: "number",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "order") as Quad_Predicate,
    labelKey: "shacl.order.label",
    descriptionKey: "shacl.order.description",
    localized: false,
    min: 0,
    type: "number",
  },
];

export const shaclPropertyTypes: ShaclProperty[] = [
  shaclDatatypeProperty,
  {
    predicate: factory.namedNode(prefixes.sh + "nodeKind") as Quad_Predicate,
    labelKey: "shacl.nodeKind.label",
    descriptionKey: "shacl.nodeKind.description",
    localized: false,
    items: nodeKinds,
    type: "url",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "class") as Quad_Predicate,
    labelKey: "shacl.class.label",
    descriptionKey: "shacl.class.description",
    localized: false,
    searchType: "class",
    type: "url",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "node") as Quad_Predicate,
    labelKey: "shacl.node.label",
    descriptionKey: "shacl.node.description",
    localized: false,
    searchType: "node",
    type: "url",
  },
  {
    predicate: factory.namedNode(
      prefixes.sh + "qualifiedValueShape",
    ) as Quad_Predicate,
    labelKey: "shacl.qualifiedValueShape.label",
    descriptionKey: "shacl.qualifiedValueShape.description",
    localized: false,
    searchType: "node",
    type: "url",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "in") as Quad_Predicate,
    labelKey: "shacl.in.label",
    descriptionKey: "shacl.in.description",
    localized: false,
    type: "list",
    listType: "linked",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "hasValue") as Quad_Predicate,
    labelKey: "shacl.hasValue.label",
    descriptionKey: "shacl.hasValue.description",
    localized: false,
    type: "list",
    listType: "regular",
  },
];

export const stringShaclProperties: ShaclProperty[] = [
  {
    predicate: factory.namedNode(
      prefixes.sh + "defaultValue",
    ) as Quad_Predicate,
    labelKey: "shacl.defaultValue.label",
    descriptionKey: "shacl.defaultValue.description",
    localized: false,
    type: "text",
  },
  {
    predicate: factory.namedNode(
      prefixes.dash + "singleLine",
    ) as Quad_Predicate,
    labelKey: "shacl.singleLine.label",
    descriptionKey: "shacl.singleLine.description",
    localized: false,
    type: "boolean",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "minLength") as Quad_Predicate,
    labelKey: "shacl.minLength.label",
    descriptionKey: "shacl.minLength.description",
    localized: false,
    max: factory.namedNode(prefixes.sh + "maxLength"),
    min: 0,
    type: "number",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "maxLength") as Quad_Predicate,
    labelKey: "shacl.maxLength.label",
    descriptionKey: "shacl.maxLength.description",
    localized: false,
    min: factory.namedNode(prefixes.sh + "minLength"),
    type: "number",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "pattern") as Quad_Predicate,
    labelKey: "shacl.pattern.label",
    descriptionKey: "shacl.pattern.description",
    localized: false,
    type: "text",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "flags") as Quad_Predicate,
    labelKey: "shacl.flags.label",
    descriptionKey: "shacl.flags.description",
    localized: false,
    type: "text",
  },
];

export const valueShaclProperties: ShaclProperty[] = [
  {
    predicate: factory.namedNode(
      prefixes.sh + "minExclusive",
    ) as Quad_Predicate,
    labelKey: "shacl.minExclusive.label",
    descriptionKey: "shacl.minExclusive.description",
    localized: false,
    min: 0,
    type: "number",
  },
  {
    predicate: factory.namedNode(
      prefixes.sh + "minInclusive",
    ) as Quad_Predicate,
    labelKey: "shacl.minInclusive.label",
    descriptionKey: "shacl.minInclusive.description",
    localized: false,
    min: 0,
    type: "number",
  },
  {
    predicate: factory.namedNode(
      prefixes.sh + "maxExclusive",
    ) as Quad_Predicate,
    labelKey: "shacl.maxExclusive.label",
    descriptionKey: "shacl.maxExclusive.description",
    localized: false,
    min: 0,
    type: "number",
  },
  {
    predicate: factory.namedNode(
      prefixes.sh + "maxInclusive",
    ) as Quad_Predicate,
    labelKey: "shacl.maxInclusive.label",
    descriptionKey: "shacl.maxInclusive.description",
    localized: false,
    min: 0,
    type: "number",
  },
];

export const administrativeShaclProperties: ShaclProperty[] = [
  {
    predicate: factory.namedNode(prefixes.sh + "path") as Quad_Predicate,
    labelKey: "shacl.path.label",
    descriptionKey: "shacl.path.description",
    localized: false,
    type: "url",
    deletable: false,
  },
  {
    predicate: factory.namedNode(prefixes.sh + "description") as Quad_Predicate,
    labelKey: "shacl.description.label",
    descriptionKey: "shacl.description.description",
    localized: true,
    multiline: true,
    type: "text",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "message") as Quad_Predicate,
    labelKey: "shacl.message.label",
    descriptionKey: "shacl.message.description",
    localized: true,
    type: "text",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "severity") as Quad_Predicate,
    labelKey: "shacl.severity.label",
    descriptionKey: "shacl.severity.description",
    localized: false,
    items: severities,
    type: "url",
  },
];

export const propertyPairProperties: ShaclProperty[] = [
  {
    predicate: factory.namedNode(prefixes.sh + "equals") as Quad_Predicate,
    labelKey: "shacl.equals.label",
    descriptionKey: "shacl.equals.description",
    localized: false,
    type: "url",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "disjoint") as Quad_Predicate,
    labelKey: "shacl.disjoint.label",
    descriptionKey: "shacl.disjoint.description",
    localized: false,
    type: "url",
  },
  {
    predicate: factory.namedNode(prefixes.sh + "lessThan") as Quad_Predicate,
    labelKey: "shacl.lessThan.label",
    descriptionKey: "shacl.lessThan.description",
    localized: false,
    type: "url",
  },
  {
    predicate: factory.namedNode(
      prefixes.sh + "lessThanOrEquals",
    ) as Quad_Predicate,
    labelKey: "shacl.lessThanOrEquals.label",
    descriptionKey: "shacl.lessThanOrEquals.description",
    localized: false,
    type: "url",
  },
];

export const qualifiedProperties: ShaclProperty[] = [
  {
    predicate: factory.namedNode(
      prefixes.sh + "qualifiedMinCount",
    ) as Quad_Predicate,
    labelKey: "shacl.qualifiedMinCount.label",
    descriptionKey: "shacl.qualifiedMinCount.description",
    localized: false,
    max: factory.namedNode(prefixes.sh + "qualifiedMaxCount"),
    min: 0,
    type: "number",
  },
  {
    predicate: factory.namedNode(
      prefixes.sh + "qualifiedMaxCount",
    ) as Quad_Predicate,
    labelKey: "shacl.qualifiedMaxCount.label",
    descriptionKey: "shacl.qualifiedMaxCount.description",
    localized: false,
    min: factory.namedNode(prefixes.sh + "qualifiedMinCount"),
    type: "number",
  },
  {
    predicate: factory.namedNode(
      prefixes.sh + "qualifiedValueShapesDisjoint",
    ) as Quad_Predicate,
    labelKey: "shacl.qualifiedValueShapesDisjoint.label",
    descriptionKey: "shacl.qualifiedValueShapesDisjoint.description",
    localized: false,
    type: "boolean",
  },
];
