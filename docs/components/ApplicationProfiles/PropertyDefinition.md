# PropertyDefinition

## Props

| Prop name          | Description | Type               | Values | Default |
| ------------------ | ----------- | ------------------ | ------ | ------- |
| applicationProfile |             | ApplicationProfile | -      |         |
| definition         |             | Dataset            | -      |         |
| readonly           |             | boolean            | -      | false   |
| selectedProperty   |             | Quad_Subject       | -      |         |

## Events

| Event name       | Properties | Description |
| ---------------- | ---------- | ----------- |
| updateDefinition |            |

---

```vue live
<PropertyDefinition
  :applicationProfile="Default Example Usage"
  :definition="Default Example Usage"
  :selectedProperty="Default Example Usage"
/>
```
