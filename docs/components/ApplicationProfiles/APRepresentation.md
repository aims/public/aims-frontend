# APRepresentation

## Props

| Prop name                  | Description | Type                                                                                                                                                                           | Values | Default   |
| -------------------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------ | --------- |
| applicationProfile         |             | ApplicationProfile                                                                                                                                                             | -      |           |
| baseApplicationProfile     |             | ApplicationProfile                                                                                                                                                             | -      |           |
| baseDefinition             |             | Dataset                                                                                                                                                                        | -      |           |
| completeApplicationProfile |             | {<br/> definition: Dataset;<br/> inheritedParts: Record&lt;<br/> string,<br/> {<br/> applicationProfile: ApplicationProfile;<br/> definition: Dataset;<br/> }<br/> &gt;;<br/>} | -      |           |
| definition                 |             | Dataset                                                                                                                                                                        | -      |           |
| interactive                |             | boolean                                                                                                                                                                        | -      | true      |
| queryingApplicationProfile |             | boolean                                                                                                                                                                        | -      | false     |
| readonly                   |             | boolean                                                                                                                                                                        | -      | false     |
| selectedProperty           |             | Quad_Subject \| undefined                                                                                                                                                      | -      | undefined |

## Events

| Event name                        | Properties                                     | Description |
| --------------------------------- | ---------------------------------------------- | ----------- |
| openEditModal                     |                                                |
| removeProperty                    |                                                |
| selectProperty                    |                                                |
| switchDisplayedApplicationProfile |                                                |
| switchInteractivity               |                                                |
| textDefinitionUpdate              | **&lt;anonymous1&gt;** `undefined` - undefined |

---

```vue live
<APRepresentation
  :applicationProfile="Default Example Usage"
  :baseApplicationProfile="Default Example Usage"
  :baseDefinition="Default Example Usage"
  :completeApplicationProfile="Default Example Usage"
  :definition="Default Example Usage"
/>
```
