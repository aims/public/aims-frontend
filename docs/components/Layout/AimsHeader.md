# AimsHeader

## Props

| Prop name | Description | Type   | Values | Default |
| --------- | ----------- | ------ | ------ | ------- |
| locale    |             | string | -      | "en"    |

## Events

| Event name   | Properties | Description |
| ------------ | ---------- | ----------- |
| changeLocale |            |

---

```vue live
<AimsHeader />
```
