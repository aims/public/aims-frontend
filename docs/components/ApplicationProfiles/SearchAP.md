# SearchAP

## Props

| Prop name                        | Description | Type   | Values | Default |
| -------------------------------- | ----------- | ------ | ------ | ------- |
| sessionStoredApplicationProfiles |             | number | -      | 0       |
| token                            |             | string | -      | ""      |

## Events

| Event name          | Properties | Description |
| ------------------- | ---------- | ----------- |
| searching           |            |
| applicationProfiles |            |

---

```vue live
<SearchAP />
```
