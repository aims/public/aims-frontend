import type { defineComponent } from "vue";

declare interface CustomUIElements {
  AfterStoreModal?: ReturnType<typeof defineComponent>;
  AuthorizationModal?: ReturnType<typeof defineComponent>;
  CustomHeaderBanner?: ReturnType<typeof defineComponent>;
  CustomFooter?: ReturnType<typeof defineComponent>;
  ReviewModal?: ReturnType<typeof defineComponent>;
  StoreDescription?: ReturnType<typeof defineComponent>;
  StoreToast?: ReturnType<typeof defineComponent>;
}

export = CustomUIElements;
