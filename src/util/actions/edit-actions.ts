import { ApplicationProfile } from "@/types/application-profile";
import factory from "rdf-ext";
import {
  addTripleIfNotEmpty,
  changeBaseUrl,
  generateAPUri,
  prefixes,
  removeOldProvenanceInformation,
} from "@/util/linkedData";
import type { Dataset } from "@rdfjs/types";

export function copyApplicationProfileAction(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
): void {
  const newBaseUrl = generateAPUri();
  const oldBaseUrl = applicationProfile.base_url;
  removeOldProvenanceInformation(applicationProfile, dataset);
  changeBaseUrl(applicationProfile, dataset, newBaseUrl, false);
  addTripleIfNotEmpty(
    factory.namedNode(newBaseUrl),
    factory.namedNode(prefixes.prov + "wasDerivedFrom"),
    factory.namedNode(oldBaseUrl),
    dataset,
  );
}

export function extendApplicationProfileAction(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
): void {
  const newBaseUrl = generateAPUri();
  const oldBaseUrl = applicationProfile.base_url;
  dataset.deleteMatches();
  changeBaseUrl(applicationProfile, dataset, newBaseUrl, false);
  addTripleIfNotEmpty(
    factory.namedNode(newBaseUrl),
    factory.namedNode(prefixes.sh + "node"),
    factory.namedNode(oldBaseUrl),
    dataset,
  );
  addTripleIfNotEmpty(
    factory.namedNode(newBaseUrl),
    factory.namedNode(prefixes.owl + "imports"),
    factory.namedNode(oldBaseUrl),
    dataset,
  );
  addTripleIfNotEmpty(
    factory.namedNode(newBaseUrl),
    factory.namedNode(prefixes.rdf + "type"),
    factory.namedNode(prefixes.sh + "NodeShape"),
    dataset,
  );
}

function getTimeStamp(base_url: string): number {
  let base_url_split = base_url.slice(0, Math.max(0, base_url.length - 1));
  base_url_split = base_url_split.slice(
    Math.max(0, base_url_split.lastIndexOf("/") + 1),
  );
  return Number(base_url_split);
}

function hasTimeStamp(base_url: string): boolean {
  const timeStamp = getTimeStamp(base_url);
  return !Number.isNaN(timeStamp) && Number.isInteger(timeStamp);
}

function replaceTimeStamp(base_url: string, timeStamp: number): string {
  const oldTimeStamp = getTimeStamp(base_url);
  return base_url.replace(oldTimeStamp.toString(), timeStamp.toString());
}

function addTimeStamp(base_url: string, timeStamp: number): string {
  return base_url + timeStamp + "/";
}

function addSlashIfNotExists(base_url: string): string {
  if (!base_url.endsWith("/")) {
    return base_url + "/";
  }
  return base_url;
}

export function newVersionApplicationProfileAction(
  applicationProfile: ApplicationProfile,
  dataset: Dataset,
): void {
  const workingBaseUrl = addSlashIfNotExists(applicationProfile.base_url);
  const newBaseUrl = hasTimeStamp(workingBaseUrl)
    ? replaceTimeStamp(workingBaseUrl, Date.now())
    : addTimeStamp(workingBaseUrl, Date.now());
  const oldBaseUrl = applicationProfile.base_url;
  removeOldProvenanceInformation(applicationProfile, dataset);
  changeBaseUrl(applicationProfile, dataset, newBaseUrl, false);
  addTripleIfNotEmpty(
    factory.namedNode(newBaseUrl),
    factory.namedNode(prefixes.prov + "wasRevisionOf"),
    factory.namedNode(oldBaseUrl),
    dataset,
  );
}
