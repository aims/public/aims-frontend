declare interface VocabularyTerm {
  short_form: string;
  vocabulary?: string;
  uri: string;
  label?: string;
  prefix?: string;
  description?: { en: string | undefined; de: string | undefined };
  ranges?: string[];
  definition?: Array<unknown>;
}

export = VocabularyTerm;
