# GraphLayout

## Props

| Prop name   | Description | Type    | Values | Default                                                                                                                    |
| ----------- | ----------- | ------- | ------ | -------------------------------------------------------------------------------------------------------------------------- |
| nodes       |             | array   | -      |                                                                                                                            |
| links       |             | array   | -      |                                                                                                                            |
| activeLinks |             | array   | -      |                                                                                                                            |
| autoZoom    |             | boolean | -      | true                                                                                                                       |
| layoutCfg   |             | object  | -      | {<br/> rankdir: "RL",<br/> align: undefined,<br/> nodesep: 20,<br/> ranksep: 50,<br/> marginx: 10,<br/> marginy: 10,<br/>} |

## Events

| Event name | Properties | Description |
| ---------- | ---------- | ----------- |
| click      |            |
| link-enter |            |
| link-out   |            |

## Slots

| Name    | Description | Bindings |
| ------- | ----------- | -------- |
| node    |             |          |
| default |             |          |

---

```vue live
<GraphLayout
  :nodes="[1, 2, 3]"
  :links="[1, 2, 3]"
  :activeLinks="[1, 2, 3]"
>Default Example Usage</GraphLayout>
```
