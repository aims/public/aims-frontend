# RemoveDraftModal

## Props

| Prop name | Description | Type    | Values | Default        |
| --------- | ----------- | ------- | ------ | -------------- |
| name      |             | string  | -      |                |
| open      |             | boolean | -      | () =&gt; false |

## Events

| Event name | Properties | Description |
| ---------- | ---------- | ----------- |
| close      |            |
| save       |            |

---

```vue live
<RemoveDraftModal name="Default Example Usage" :open="true" />
```
