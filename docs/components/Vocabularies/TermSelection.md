# TermSelection

## Props

| Prop name | Description | Type    | Values | Default |
| --------- | ----------- | ------- | ------ | ------- |
| readonly  |             | boolean | -      | false   |

## Events

| Event name        | Properties | Description |
| ----------------- | ---------- | ----------- |
| addVocabularyTerm |            |

---

```vue live
<TermSelection />
```
